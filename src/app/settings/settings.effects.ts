import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { select, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { interval, merge, of } from 'rxjs';
import { distinctUntilChanged, map, mapTo, tap, withLatestFrom } from 'rxjs/operators';

// @ts-ignore
import { AnimationsService, AppState, LocalStorageService, TitleService } from '@app/core';

import { ActionSettingsChangeHour, SettingsActions, SettingsActionTypes } from './settings.actions';
import { selectEffectiveTheme, selectSettingsState } from './settings.selectors';
import { State } from './settings.model';

export const SETTINGS_KEY = 'SETTINGS';

const INIT = of('qc-init-effect-trigger');

@Injectable()
export class SettingsEffects {
    constructor(
        private actions$: Actions<SettingsActions>,
        private store: Store<AppState>,
        private router: Router,
        private overlayContainer: OverlayContainer,
        private localStorageService: LocalStorageService,
        private titleService: TitleService,
        private animationsService: AnimationsService,
    ) {}

    @Effect()
    changeHour = interval(60_000).pipe(
        mapTo(new Date().getHours()),
        distinctUntilChanged(),
        map(hour => new ActionSettingsChangeHour({ hour }))
    );

    @Effect({ dispatch: false })
    persistSettings = this.actions$.pipe(
        ofType(
            SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS,
            SettingsActionTypes.CHANGE_ANIMATIONS_PAGE,
            SettingsActionTypes.CHANGE_ANIMATIONS_PAGE_DISABLED,
            SettingsActionTypes.CHANGE_AUTO_NIGHT_AUTO_MODE,
            SettingsActionTypes.CHANGE_LANGUAGE,
            SettingsActionTypes.CHANGE_STICKY_HEADER,
            SettingsActionTypes.CHANGE_THEME
        ),
        withLatestFrom(this.store.pipe(select(selectSettingsState))),
        tap(([action, settings]) =>
            this.localStorageService.setItem(SETTINGS_KEY, settings)
        )
    );

    @Effect({ dispatch: false })
    updateRouteAnimationType = merge(
        INIT,
        this.actions$.pipe(
            ofType(
                SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS,
                SettingsActionTypes.CHANGE_ANIMATIONS_PAGE
            )
        )
    ).pipe(
        withLatestFrom(this.store.pipe(select(selectSettingsState))),
        tap(([action, settings]) =>
            this.animationsService.updateRouteAnimationType(
                settings.pageAnimations,
                settings.elementsAnimations
            )
        )
    );

    @Effect({ dispatch: false })
    updateTheme = merge(
        INIT,
        this.actions$.pipe(ofType(SettingsActionTypes.CHANGE_THEME))
    ).pipe(
        withLatestFrom(this.store.pipe(select(selectEffectiveTheme))),
        tap(([action, effectiveTheme]) => {
            const classList = this.overlayContainer.getContainerElement().classList;
            const toRemove = Array.from(classList).filter((item: string) =>
                item.includes('-theme')
            );
            if (toRemove.length) {
                classList.remove(...toRemove);
            }
            classList.add(effectiveTheme);
        })
    );
}
