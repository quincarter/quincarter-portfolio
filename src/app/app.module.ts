import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '@env/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QcNavComponent } from './components/qc-nav/qc-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { APP_BASE_HREF } from '@angular/common';
import { AngularFireModule } from '@angular/fire';
import { QcAboutMeModule } from './components/qc-about-me/qc-about-me.module';
import { CoreModule } from '@app/core';
import { AppRoutingModule } from './app-routing.module';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';

@NgModule({
    declarations: [
        AppComponent,
        QcNavComponent,
    ],
    imports: [
        QcAboutMeModule,
        CoreModule,
        AngularFireModule.initializeApp(environment.firebase),
        BrowserAnimationsModule,
        BrowserModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        FlexLayoutModule,
        LayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatGridListModule,
        NgxAuthFirebaseUIModule.forRoot(
            environment.firebase,
            null,
            {enableFirestoreSync: true}
        ),
        AppRoutingModule,
    ],
    providers: [{
        provide: APP_BASE_HREF,
        useValue: '/'
    }],
    bootstrap: [AppComponent],
})
export class AppModule {
}
