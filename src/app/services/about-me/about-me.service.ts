import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AboutMeService {

    private _aboutMe: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly aboutMe$: Observable<any> = this._aboutMe.asObservable();

    constructor(
        private db: AngularFirestore
    ) {
    }

    public getAboutMe(): void {
        this._aboutMe.next(this.db.collection('/about_me').valueChanges());
    }
}
