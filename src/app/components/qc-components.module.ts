import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
    path: '',
    loadChildren: () => import('./qc-dashboard/qc-dashboard.module').then(m => m.QcDashboardModule)
}, {
    path: 'about',
    loadChildren: () => import('./qc-about-me/qc-about-me.module').then(m => m.QcAboutMeModule)
}, {
    path: 'projects',
    loadChildren: () => import('./qc-projects/qc-projects.module').then(m => m.QcProjectsModule)
}, {
    path: 'sample-auth',
    loadChildren: () => import('./qc-auth/qc-auth.module').then(m => m.QcAuthModule)
}, {
    path: 'career',
    loadChildren: () => import('./qc-experience/qc-experience.module').then(m => m.QcExperienceModule)
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    declarations: [],
    exports: [],
})
export class QcComponentsModule {
}
