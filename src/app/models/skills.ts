export interface Skills {
    frameworks: ISkillGroup;
    languages: ISkillGroup;
}

export interface ISkillGroup {
    image_name: string;
    name: string;
}
