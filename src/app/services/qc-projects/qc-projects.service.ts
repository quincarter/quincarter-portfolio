import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class QcProjectsService {

    private _projects: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly projects$: Observable<any> = this._projects.asObservable();

    public gitlabBaseUrl = 'https://gitlab.com/api/v4/';
    public xhr = new XMLHttpRequest();

    constructor(
        private http: HttpClient,
    ) {
    }

    public getProjects(): void {
        this._projects.next(this.http.get(this.gitlabBaseUrl + 'users/quincarter/projects'));
    }
}
