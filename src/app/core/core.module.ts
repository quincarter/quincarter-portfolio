import { NgModule, Optional, SkipSelf, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
    StoreRouterConnectingModule,
    RouterStateSerializer
} from '@ngrx/router-store';

import { httpInterceptorProviders } from './http-interceptors';
import { LocalStorageService } from './local-storage/local-storage.service';
import { AuthEffects } from './auth/auth.effects';
import { AuthGuardService } from './auth/auth-guard.service';
import { AnimationsService } from './animations/animations.service';
import { TitleService } from './title/title.service';
import { reducers, metaReducers } from './core.state';
import { AppErrorHandler } from './error-handler/app-error-handler.service';
import { CustomSerializer } from './router/custom-serializer';
import { NotificationService } from './notifications/notification.service';
import { GoogleAnalyticsEffects } from './google-analytics/google-analytics.effects';
// @ts-ignore
import { environment } from '@env/environment';

@NgModule({
    imports: [
        // angular
        CommonModule,
        HttpClientModule,

        // ngrx
        StoreModule.forRoot(reducers, {metaReducers}),
        StoreRouterConnectingModule.forRoot(),
        EffectsModule.forRoot([AuthEffects, GoogleAnalyticsEffects]),
        environment.production
            ? []
            : StoreDevtoolsModule.instrument({
                name: 'Angular NgRx Material Starter'
            })
    ],
    declarations: [],
    providers: [
        NotificationService,
        LocalStorageService,
        AuthGuardService,
        AnimationsService,
        httpInterceptorProviders,
        TitleService,
        {provide: ErrorHandler, useClass: AppErrorHandler},
        {provide: RouterStateSerializer, useClass: CustomSerializer}
    ],
    exports: []
})
export class CoreModule {
    constructor(
        @Optional()
        @SkipSelf()
            parentModule: CoreModule
    ) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import only in AppModule');
        }
    }
}
