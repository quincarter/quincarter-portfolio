import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { QcProfileCardComponent } from './qc-profile-card.component';
import { ClipboardModule } from 'ngx-clipboard';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        QcProfileCardComponent,
    ],
    exports: [
        QcProfileCardComponent,
    ],
    imports: [
        CommonModule,
        ClipboardModule,
        FlexLayoutModule,
        HttpClientModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatGridListModule,
        MatCardModule,
        MatRippleModule,
        MatSnackBarModule,
    ],
    providers: [],
})
export class QcProfileModule {
}
