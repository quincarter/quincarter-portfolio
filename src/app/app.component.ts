import { Component, OnInit } from '@angular/core';
import { routeAnimations } from '@app/core';
import browser from 'browser-detect';
import { LocalStorageService } from '@app/core';
import { ActionSettingsChangeAnimationsPageDisabled } from './settings';
import { Store } from '@ngrx/store';
import { AppState } from '@app/core';

@Component({
    selector: 'qc-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    animations: [routeAnimations]
})
export class AppComponent implements OnInit {
    title = 'Quin Carter - Senior Software Engineer';

    private static isIEorEdgeOrSafari() {
        return ['ie', 'edge', 'safari'].includes(browser().name);
    }

    constructor(
        private store: Store<AppState>,
        private storageService: LocalStorageService
    ) {
    }

    ngOnInit(): void {
        this.storageService.testLocalStorage();
        if (AppComponent.isIEorEdgeOrSafari()) {
            this.store.dispatch(
                new ActionSettingsChangeAnimationsPageDisabled({
                    pageAnimationsDisabled: true
                })
            );
        }
    }
}
