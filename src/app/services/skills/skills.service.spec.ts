import { inject, TestBed } from '@angular/core/testing';

import { SkillsService } from './skills.service';
import { BehaviorSubject } from 'rxjs';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';

const skillsMock: any = {
    data: {
        frameworks: [
            'Angular 2+',
            'ReactJS',
            'Vue',
            'Node/Express'
        ],
        languages: [
            'JavaScript',
            'TypeScript',
            'NodeJS'
        ]
    }
};

const FirestoreStub: any = {
    collection: (name: string) => ({
        doc: (_id: string) => ({
            valueChanges: () => new BehaviorSubject(skillsMock),
            set: (_d: any) => new Promise((resolve, _reject) => resolve()),
        }),
    }),
};

describe('SkillsService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            AngularFirestoreModule
        ],
        providers: [
            {provide: AngularFirestore, useValue: FirestoreStub},
        ],
    }));
    beforeEach(
        inject([SkillsService, AngularFirestore], (_service, _httpMock) => {
            const service = _service;
            const httpMock = _httpMock;
        }));

    it('should be created', () => {
        const service: SkillsService = TestBed.get(SkillsService);
        expect(service).toBeTruthy();
    });

    it('should get data', () => {
        const service: SkillsService = TestBed.get(SkillsService);
        expect(service).toBeTruthy();
        expect(FirestoreStub).toBe(FirestoreStub, 'FirestoreStub Does not match');
    });
});
