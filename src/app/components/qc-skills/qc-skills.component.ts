import { Component, Input } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';

@Component({
    selector: 'qc-skills',
    templateUrl: './qc-skills.component.html',
    styleUrls: ['./qc-skills.component.scss']
})
export class QcSkillsComponent {

    @Input() skillsColspan: number;
    @Input() skillsRowspan: number;
    @Input() skills: any;

    public numCols = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({matches}) => {
            if (matches) {
                return 2;
            }
            return this.skills.languages.length;
        })
    );

    public numColsFrameworks = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({matches}) => {
            if (matches) {
                return 2;
            }
            return this.skills.frameworks.length;
        })
    );


    public assetPath = '../../../assets/img/';

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {
    }
}
