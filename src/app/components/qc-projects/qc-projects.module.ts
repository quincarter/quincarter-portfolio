import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { QcProjectsService } from '../../services/qc-projects/qc-projects.service';
import { QcProjectsComponent } from './qc-projects.component';
import { RouterModule, Routes } from '@angular/router';
import { MomentModule } from 'ngx-moment';
import { QcProjectsResolver } from './qc-projects.resolver';
import { CommonModule } from '@angular/common';

const routes: Routes = [{
    path: '',
    component: QcProjectsComponent,
    resolve: {
        resolver: QcProjectsResolver,
    }
}];

@NgModule({
    declarations: [
        QcProjectsComponent,
    ],
    exports: [
        QcProjectsComponent,
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        MatButtonModule,
        MatCardModule,
        MatIconModule,
        MatGridListModule,
        MatTableModule,
        MomentModule.forRoot({
            relativeTimeThresholdOptions: {
                'm': 59
            }
        }),
        RouterModule.forChild(routes),
    ],
    providers: [
        QcProjectsResolver,
        QcProjectsService,
    ],
})
export class QcProjectsModule {
}
