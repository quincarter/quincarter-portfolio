export interface Experience {
    company_name: string;
    duties: any;
    start_date: string;
    end_date?: string;
    job_title: string;
    logo: string;
}
