import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcProjectsComponent } from './qc-projects.component';
import { environment } from '../../../environments/environment';
import { CommonModule } from '@angular/common';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MomentModule } from 'ngx-moment';
import { QcProjectsResolver } from './qc-projects.resolver';
import { QcProjectsService } from '../../services/qc-projects/qc-projects.service';

describe('QcProjectsComponent', () => {
    let component: QcProjectsComponent;
    let fixture: ComponentFixture<QcProjectsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [QcProjectsComponent],
            imports: [
                CommonModule,
                HttpClientModule,
                MatButtonModule,
                MatCardModule,
                MatIconModule,
                MatGridListModule,
                MatTableModule,
                MomentModule.forRoot({
                    relativeTimeThresholdOptions: {
                        'm': 59
                    }
                }),
            ],
            providers: [
                QcProjectsResolver,
                QcProjectsService,
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QcProjectsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
