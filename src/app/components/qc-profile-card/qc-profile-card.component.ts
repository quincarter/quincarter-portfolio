import { Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ROUTE_ANIMATIONS_ELEMENTS, routeAnimations } from '../../core/animations/route.animations';

@Component({
    selector: 'qc-profile-card',
    templateUrl: './qc-profile-card.component.html',
    styleUrls: ['./qc-profile-card.component.scss'],
    animations: [routeAnimations]
})
export class QcProfileCardComponent {
    public routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

    @Input() profileColspan: number;
    @Input() profileRowspan: number;
    @Input() profile: any;

    constructor(
        public snackBar: MatSnackBar,
    ) {
    }

    public copied(event): void {
        this.openSnackBar(event.content);
    }

    public openSnackBar(content) {
        this.snackBar.open(`Copied ${content} to the clipboard`, 'CLOSE', {
            duration: 3000
        });
    }
}
