import { NgModule } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { QcSkillsComponent } from './qc-skills.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        QcSkillsComponent,
    ],
    exports: [
        QcSkillsComponent,
    ],
    imports: [
        CommonModule,
        MatGridListModule,
    ],
    providers: [],
})
export class QcSkillsModule {
}
