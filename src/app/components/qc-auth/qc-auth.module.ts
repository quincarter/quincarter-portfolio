import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QcAuthComponent } from './qc-auth.component';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
    path: 'sample-auth',
    component: QcAuthComponent,
}];

@NgModule({
    declarations: [
        QcAuthComponent
    ],
    imports: [
        CommonModule,
        NgxAuthFirebaseUIModule,
        RouterModule.forChild(routes),
    ],
    exports: [
        QcAuthComponent
    ],
    providers: [
        NgxAuthFirebaseUIModule
    ]
})
export class QcAuthModule {
}
