import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcSkillsComponent } from './qc-skills.component';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';

describe('QcSkillsComponent', () => {
    let component: QcSkillsComponent;
    let fixture: ComponentFixture<QcSkillsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                QcSkillsComponent
            ],
            imports: [
                CommonModule,
                MatGridListModule,
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {

        fixture = TestBed.createComponent(QcSkillsComponent);
        // snapshot
        fixture.componentInstance.skills = {
            languages: [
                {
                    image_name: 'javascript.png',
                    name: 'JavaScript'
                },
                {
                    image_name: 'typescript.png',
                    name: 'TypeScript'
                }
            ],
            frameworks: [
                {
                    image_name: 'angular.png',
                    name: 'Angular'
                },
                {
                    image_name: 'tsed.png',
                    name: 'TS.ED'
                }
            ]
        };
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
