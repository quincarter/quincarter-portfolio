import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { QcProjectsService } from '../../services/qc-projects/qc-projects.service';

@Injectable()
export class QcProjectsResolver implements Resolve<any> {

    constructor(
        private projectService: QcProjectsService,
    ) {
    }

    _init(): void {
        this.getProjectsData();
    }

    resolve(): Observable<any[]> | Promise<any[]> {
        return new Promise((resolve, reject) => {
            Promise.all([
                this._init(),
            ]).then(data => {
                resolve(data);
            }, reject);
        });
    }

    private getProjectsData(): void {
        this.projectService.getProjects();
    }
}
