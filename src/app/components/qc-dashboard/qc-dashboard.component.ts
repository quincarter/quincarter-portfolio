import { Component, OnDestroy, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { DashboardService } from '@app/services/dashboard/dashboard.service';
import { Observable, Subscription } from 'rxjs';
import { ROUTE_ANIMATIONS_ELEMENTS, routeAnimations } from '@core/animations/route.animations';

@Component({
    selector: 'qc-dashboard',
    templateUrl: './qc-dashboard.component.html',
    styleUrls: ['./qc-dashboard.component.css'],
    animations: [routeAnimations]
})
export class QcDashboardComponent implements OnInit, OnDestroy {
    public routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
    /** Based on the screen size, switch from standard to one column per row */
    cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({matches}) => {
            if (matches) {
                return [
                    {title: 'Card 1', cols: 1, rows: 1},
                    {title: 'Card 2', cols: 1, rows: 1},
                    {title: 'Card 3', cols: 1, rows: 1},
                    {title: 'Card 4', cols: 1, rows: 1}
                ];
            }

            return [
                {title: 'Card 1', cols: 2, rows: 1},
                {title: 'Card 2', cols: 1, rows: 1},
                {title: 'Card 3', cols: 1, rows: 2},
                {title: 'Card 4', cols: 1, rows: 1}
            ];
        })
    );

    public profileCard = {
        cols: 2,
        rows: 1
    };

    public skillsCard = {
        cols: 1,
        rows: 1
    };

    public dashboard$: Observable<any>;

    private dashboardSubscription: Subscription;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private dashboardService: DashboardService
    ) {
    }

    ngOnInit(): void {
        this.getDashboardSubscription();
    }

    ngOnDestroy(): void {
        this.dashboardSubscription.unsubscribe();
    }

    private getDashboardSubscription(): void {
        this.dashboardSubscription = this.dashboardService.dashboard$.subscribe(data => {
            this.dashboard$ = data;
        });
    }
}
