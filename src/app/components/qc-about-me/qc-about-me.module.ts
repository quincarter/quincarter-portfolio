import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AboutMeService } from '@app/services/about-me/about-me.service';
import { QcAboutMeComponent } from './qc-about-me.component';
import { RouterModule, Routes } from '@angular/router';
import { AboutMeResolver } from './about-me.resolver';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: 'about',
        component: QcAboutMeComponent,
        resolve: {
            resolver: AboutMeResolver
        },
    }
];

@NgModule({
    declarations: [
        QcAboutMeComponent,
    ],
    exports: [
        QcAboutMeComponent
    ],
    imports: [
        AngularFirestoreModule,
        CommonModule,
        FlexLayoutModule,
        HttpClientModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatGridListModule,
        MatCardModule,
        MatRippleModule,
        RouterModule.forChild(routes),
    ],
    providers: [
        AboutMeResolver,
        AboutMeService
    ],
})
export class QcAboutMeModule {
}
