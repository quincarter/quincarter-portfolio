import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    private _dashboard: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly dashboard$: Observable<any> = this._dashboard.asObservable();

    constructor(
        private db: AngularFirestore
    ) {
    }

    public getDashboard(): void {
        this._dashboard.next(this.db.collection('/dashboard').valueChanges());
    }
}
