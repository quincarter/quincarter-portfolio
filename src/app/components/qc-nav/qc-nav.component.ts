import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ROUTE_ANIMATIONS_ELEMENTS, routeAnimations } from '../../core/animations/route.animations';


@Component({
    selector: 'qc-nav',
    templateUrl: './qc-nav.component.html',
    styleUrls: ['./qc-nav.component.scss'],
    animations: [routeAnimations]
})
export class QcNavComponent {
    public routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

    constructor(private breakpointObserver: BreakpointObserver) {
    }
}
