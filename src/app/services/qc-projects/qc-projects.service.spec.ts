import { TestBed } from '@angular/core/testing';

import { QcProjectsService } from './qc-projects.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('QcProjectsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          HttpClientTestingModule
      ]
  }));

  it('should be created', () => {
    const service: QcProjectsService = TestBed.get(QcProjectsService);
    expect(service).toBeTruthy();
  });
});
