export interface GitlabProjects {
    id: number;
    description: string;
    name: string;
    name_with_namespace: string;
    path: string;
    path_with_namespace: string;
    created_at: string;
    default_branch: string;
    tag_list: any;
    ssh_url_to_repo: string;
    http_url_to_repo: string;
    web_url: string;
    readme_url: string;
    avatar_url: string;
    star_count: number;
    forks_count: number;
    last_activity_at: string;
    namespace: GitlabNamespace;
}

export interface GitlabNamespace {
    id: number;
    name: string;
    path: string;
    kind: string;
    full_path: string;
    parent_id: number;
    avatar_url: string;
    web_url: string;
}
