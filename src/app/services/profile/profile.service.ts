import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    private _profile: BehaviorSubject<any> = new BehaviorSubject(null);
    public readonly profile$: Observable<any> = this._profile.asObservable();

    constructor(
        private db: AngularFirestore
    ) {
    }

    public getProfile(): void {
        this._profile.next(this.db.collection('/profile').valueChanges());
    }

}
