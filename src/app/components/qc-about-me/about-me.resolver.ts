import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { AboutMeService } from '../../services/about-me/about-me.service';

@Injectable()
export class AboutMeResolver implements Resolve<any> {

    constructor(
        private aboutMeService: AboutMeService,
    ) {
    }

    _init(): void {
        this.getAboutMeData();
    }

    resolve(): Observable<any[]> | Promise<any[]> {
        return new Promise((resolve, reject) => {
            Promise.all([
                this._init(),
            ]).then(data => {
                resolve(data);
            }, reject);
        });
    }

    private getAboutMeData(): void {
        this.aboutMeService.getAboutMe();
    }
}
