import { animate, state, style, transition, trigger } from '@angular/animations';
import { AnimationsService } from '@app/core/animations/animations.service';

export const detailAnimations = trigger('detailExpand', [
    state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
    state('expanded', style({height: '*'})),
    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
]);

export function isDetailAnimationsAll() {
    return AnimationsService.isDetailAnimationsType('ALL');
}

export function isDetailAnimationsNone() {
    return AnimationsService.isDetailAnimationsType('NONE');
}

export function isDetailAnimationsPage() {
    return AnimationsService.isDetailAnimationsType('PAGE');
}

export function isDetailAnimationsElements() {
    return AnimationsService.isDetailAnimationsType('ELEMENTS');
}
