import { inject, TestBed } from '@angular/core/testing';

import { AboutMeService } from './about-me.service';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs';

const aboutMeMock = {
    intro: [
        ['Some test text here'],
        ['Some more test text here'],
        ['Testing a 3rd index'],
    ]
};

const FirestoreStub = {
    collection: (name: string) => ({
        doc: (_id: string) => ({
            valueChanges: () => new BehaviorSubject(aboutMeMock),
            set: (_d: any) => new Promise((resolve, _reject) => resolve()),
        }),
    }),
};
describe('AboutMeService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            AngularFirestoreModule
        ],
        providers: [
            {provide: AngularFirestore, useValue: FirestoreStub},
        ],
    }));

    beforeEach(
        inject([AboutMeService, AngularFirestore], (_service, _httpMock) => {
            const service = _service;
            const httpMock = _httpMock;
        }));

    it('should be created', () => {
        const service: AboutMeService = TestBed.get(AboutMeService);
        expect(service).toBeTruthy();
    });
});
