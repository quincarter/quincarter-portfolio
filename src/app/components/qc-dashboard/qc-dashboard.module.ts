import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { QcDashboardComponent } from './qc-dashboard.component';
import { environment } from '../../../environments/environment';
import { QcDashboardResolver } from './qc-dashboard.resolver';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { QcProfileModule } from '../qc-profile-card/qc-profile.module';
import { QcSkillsModule } from '../qc-skills/qc-skills.module';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { QcExperienceModule } from '@app/components/qc-experience/qc-experience.module';
import { ScrollingModule } from '@angular/cdk/scrolling';

const routes: Routes = [{
    path: '',
    component: QcDashboardComponent,
    resolve: {
        resolver: QcDashboardResolver
    }
}];

@NgModule({
    declarations: [
        QcDashboardComponent,
    ],
    exports: [
        QcDashboardComponent,
    ],
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        CommonModule,
        HttpClientModule,
        MatButtonModule,
        MatIconModule,
        MatGridListModule,
        QcProfileModule,
        QcSkillsModule,
        RouterModule.forChild(routes),
        QcExperienceModule,
        ScrollingModule,
    ],
    providers: [
        QcDashboardResolver,
        DashboardService
    ],
})
export class QcDashboardModule {
}
