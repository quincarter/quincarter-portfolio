export const environment = {
        production: true,
        appName: 'Quin Carter - Software Engineer',
        test: false,
        firebase: {
            apiKey: 'AIzaSyCz6O8t7YwMJGl5stKa3L72Q-02RdIBkck',
            authDomain: 'quincarter-portfolio.firebaseapp.com',
            databaseURL: 'https://quincarter-portfolio.firebaseio.com',
            projectId: 'quincarter-portfolio',
            storageBucket: 'quincarter-portfolio.appspot.com',
            messagingSenderId: '985433744073',
            appId: '1:985433744073:web:b0a94714aab3b790'
        },
    }
;
