import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
// @ts-ignore
import { GitlabProjects } from '@app/models/gitlab';
// @ts-ignore
import { QcProjectsService } from '@app/services/qc-projects/qc-projects.service';
// @ts-ignore
import { routeAnimations } from '@app/core';
// @ts-ignore
import { detailAnimations } from '@app/core';

@Component({
    selector: 'qc-projects',
    templateUrl: './qc-projects.component.html',
    styleUrls: ['./qc-projects.component.scss'],
    animations: [routeAnimations, detailAnimations],
})
export class QcProjectsComponent implements OnInit, OnDestroy {

    public projects$: Observable<GitlabProjects[]>;
    public columnsToDisplay = ['name', 'last_activity_at'];
    public expandedElement: GitlabProjects | null;

    private projectSubscription: Subscription;

    constructor(
        private projectService: QcProjectsService
    ) {
    }

    ngOnInit(): void {
        this.getProjectSubscription();
    }

    ngOnDestroy(): void {
        this.projectSubscription.unsubscribe();
    }

    private getProjectSubscription(): void {
        this.projectSubscription = this.projectService.projects$.subscribe(data => {
            this.projects$ = data;
        });
    }
}
