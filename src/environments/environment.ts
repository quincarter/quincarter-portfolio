// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    test: false,
    appName: 'Quin Carter - Software Engineer',
    firebase: {
        apiKey: 'AIzaSyCz6O8t7YwMJGl5stKa3L72Q-02RdIBkck',
        authDomain: 'quincarter-portfolio.firebaseapp.com',
        databaseURL: 'https://quincarter-portfolio.firebaseio.com',
        projectId: 'quincarter-portfolio',
        storageBucket: 'quincarter-portfolio.appspot.com',
        messagingSenderId: '985433744073',
        appId: '1:985433744073:web:b0a94714aab3b790',
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
