import { Injectable } from '@angular/core';

@Injectable()
export class AnimationsService {
    constructor() {
        AnimationsService.routeAnimationType = 'ALL';
        AnimationsService.detailAnimationType = 'ALL';
    }

    private static routeAnimationType: RouteAnimationType = 'ALL';
    private static detailAnimationType: DetailAnimationType = 'ALL';

    static isRouteAnimationsType(type: RouteAnimationType) {
        return AnimationsService.routeAnimationType === type;
    }

    static isDetailAnimationsType(type: DetailAnimationType) {
        return AnimationsService.routeAnimationType === type;
    }

    updateRouteAnimationType(
        pageAnimations: boolean,
        elementsAnimations: boolean
    ) {
        AnimationsService.routeAnimationType =
            pageAnimations && elementsAnimations
                ? 'ALL'
                : pageAnimations
                ? 'PAGE'
                : elementsAnimations
                    ? 'ELEMENTS'
                    : 'ALL';
    }

    updateDetailAnimationType(
        pageAnimations: boolean,
        elementsAnimations: boolean
    ) {
        AnimationsService.detailAnimationType =
            pageAnimations && elementsAnimations
                ? 'ALL'
                : pageAnimations
                ? 'PAGE'
                : elementsAnimations
                    ? 'ELEMENTS'
                    : 'ALL';
    }
}

export type RouteAnimationType = 'ALL' | 'PAGE' | 'ELEMENTS' | 'NONE';
export type DetailAnimationType = 'ALL' | 'PAGE' | 'ELEMENTS' | 'NONE';
