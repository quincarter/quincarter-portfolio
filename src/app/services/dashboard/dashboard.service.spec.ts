import { TestBed } from '@angular/core/testing';

import { DashboardService } from './dashboard.service';
import { BehaviorSubject } from 'rxjs';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';

const dashboardMock = {
    profile: {
        contact: {
            email: 'test@test.com',
            github: 'https://github.com/usernameHere',
            gitlab: 'https://gitlab.com/usernameHere',
            linkedin: 'https://linkedin.com/in/usernameHere',
            phone: '(123) 456-7890',
            phone_just_numbers: '1234567890'
        },
        name: 'Some Guy',
        tagline: 'Something inspirational',
        title: 'I am super important',
        title_meta: 'I really am'
    }
};

const FirestoreStub = {
    collection: (name: string) => ({
        doc: (_id: string) => ({
            valueChanges: () => new BehaviorSubject(dashboardMock),
            set: (_d: any) => new Promise((resolve, _reject) => resolve()),
        }),
    }),
};

describe('DashboardService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            AngularFirestoreModule
        ],
        providers: [
            {provide: AngularFirestore, useValue: FirestoreStub},
        ],
    }));

    it('should be created', () => {
        const service: DashboardService = TestBed.get(DashboardService);
        expect(service).toBeTruthy();
    });
});
