import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { QcExperienceService } from '@app/services/experience/about-me.service';

@Injectable()
export class ExperienceResolver implements Resolve<any> {

    constructor(
        private experienceService: QcExperienceService,
    ) {
    }

    _init(): void {
        this.getJobExperienceData();
    }

    resolve(): Observable<any[]> | Promise<any[]> {
        return new Promise((resolve, reject) => {
            Promise.all([
                this._init(),
            ]).then(data => {
                resolve(data);
            }, reject);
        });
    }

    private getJobExperienceData(): void {
        this.experienceService.getJobExperience();
    }
}
