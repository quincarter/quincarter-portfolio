import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';

import { QcDashboardComponent } from './qc-dashboard.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { QcProfileModule } from '../qc-profile-card/qc-profile.module';
import { QcSkillsModule } from '../qc-skills/qc-skills.module';
import { QcDashboardResolver } from './qc-dashboard.resolver';
import { DashboardService } from '../../services/dashboard/dashboard.service';

describe('QcDashboardComponent', () => {
    let component: QcDashboardComponent;
    let fixture: ComponentFixture<QcDashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                QcDashboardComponent
            ],
            imports: [
                AngularFireModule.initializeApp(environment.firebase),
                AngularFirestoreModule,
                CommonModule,
                HttpClientModule,
                MatButtonModule,
                MatIconModule,
                MatGridListModule,
                QcProfileModule,
                QcSkillsModule,
            ],
            providers: [
                QcDashboardResolver,
                DashboardService
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QcDashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should compile', () => {
        expect(component).toBeTruthy();
    });
});
