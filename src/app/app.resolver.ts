import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';

import { DashboardService } from './services/dashboard/dashboard.service';
import { QcExperienceService } from '@app/services/experience/about-me.service';

@Injectable()
export class AppResolver implements Resolve<any> {

    constructor(
        private dashboardService: DashboardService,
        private experienceService: QcExperienceService,
    ) {
    }

    _init(): void {
        this.getDashboardData();
    }

    resolve(): Observable<any[]> | Promise<any[]> {
        return new Promise((resolve, reject) => {
            Promise.all([
                this._init(),
            ]).then(data => {
                resolve(data);
            }, reject);
        });
    }

    private getDashboardData(): void {
        this.dashboardService.getDashboard();
    }

    private getExperience(): void {
        this.experienceService.getJobExperience();
    }
}
