import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { QcExperienceService } from '@app/services/experience/about-me.service';
import { Observable, Subscription } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/internal/operators';

@Component({
    selector: 'qc-experience',
    templateUrl: './qc-experience.component.html',
    styleUrls: ['./qc-experience.component.scss']
})
export class QcExperienceComponent implements OnInit, OnDestroy {

    public experience$: Observable<any>;
    private experienceSubscription: Subscription;

    public popover = false;
    public toolTipText = 'Cool Story, this is actually using Angular\'s new Virtual Scrolling capabilities';

    public numCols = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({matches}) => {
            if (matches) {
                return 1;
            } else {
                return 4;
            }
        })
    );

    constructor(
        private breakpointObserver: BreakpointObserver,
        private experienceService: QcExperienceService
    ) {
    }


    ngOnInit() {
        this.getExperienceSubscription();
    }

    ngOnDestroy(): void {
        this.experienceSubscription.unsubscribe();
    }

    showPopOver(): void {
        this.popover = true;
        setTimeout(() => {
            this.popover = false;
        }, 3000);
    }

    private getExperienceSubscription(): void {
        this.experienceSubscription = this.experienceService.experience$.subscribe(data => {
            this.experience$ = data;
        });
    }
}
