import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qc-auth',
  templateUrl: './qc-auth.component.html',
  styleUrls: ['./qc-auth.component.scss']
})
export class QcAuthComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  profileSuccess(event: Event): void {
      console.log(event);
  }

  profileError(event: Event): void {
      console.log(event);
  }
}
