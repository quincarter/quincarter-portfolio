export class Profile {
    constructor(name: string,
                tagline: string,
                title: string,
                title_meta: string
    ) {
    }
}

export interface IProfile {
    name: string;
    tagline: string;
    title: string;
    title_meta: string;
}
