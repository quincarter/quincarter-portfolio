import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class QcExperienceService {

    private _experience: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly experience$: Observable<any> = this._experience.asObservable();

    constructor(
        private db: AngularFirestore
    ) {
    }

    public getJobExperience(): void {
        this._experience.next(this.db.collection('/job_experience').valueChanges());
    }
}
