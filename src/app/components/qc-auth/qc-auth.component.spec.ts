import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcAuthComponent } from './qc-auth.component';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { environment } from '../../../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('QcAuthComponent', () => {
    let component: QcAuthComponent;
    let fixture: ComponentFixture<QcAuthComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [QcAuthComponent],
            imports: [
                BrowserAnimationsModule,
                NgxAuthFirebaseUIModule.forRoot(environment.firebase)
            ],
            providers: [
                NgxAuthFirebaseUIModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QcAuthComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
