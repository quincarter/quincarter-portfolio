import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';

describe('NotificationsService', () => {
  let service: NotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationService, MatSnackBar, Overlay]
    });
    service = TestBed.get(NotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('default method should be executable', () => {
    jest.spyOn(service, 'default').mockImplementation(() => {});
    service.default('default message');
    expect(service.default).toHaveBeenCalled();
  });

  it('info method should be executable', () => {
    jest.spyOn(service, 'info').mockImplementation(() => {});
    service.info('info message');
    expect(service.info).toHaveBeenCalled();
  });

  it('success method should be executable', () => {
    jest.spyOn(service, 'success').mockImplementation(() => {});
    service.success('success message');
    expect(service.success).toHaveBeenCalled();
  });

  it('warning method should be executable', () => {
    jest.spyOn(service, 'warn').mockImplementation(() => {});
    service.warn('warning message');
    expect(service.warn).toHaveBeenCalled();
  });

  it('error method should be executable', () => {
    jest.spyOn(service, 'error').mockImplementation(() => {});
    service.error('error message');
    expect(service.error).toHaveBeenCalled();
  });
});
