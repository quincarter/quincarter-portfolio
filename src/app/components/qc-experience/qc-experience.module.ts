import { NgModule } from '@angular/core';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { RouterModule, Routes } from '@angular/router';
import { QcExperienceComponent } from '@app/components/qc-experience/qc-experience.component';
import { QcExperienceService } from '@app/services/experience/about-me.service';
import { ExperienceResolver } from '@app/components/qc-experience/experience.resolver';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ScrollingModule } from '@angular/cdk/scrolling';

const routes: Routes = [
    {
        path: 'career',
        component: QcExperienceComponent,
        resolve: {
            resolver: ExperienceResolver
        },
    }
];

@NgModule({
    declarations: [
        QcExperienceComponent,
    ],
    exports: [
        QcExperienceComponent
    ],
    imports: [
        CommonModule,
        AngularFirestoreModule,
        RouterModule.forChild(routes),
        MatCardModule,
        ScrollingModule,
        MatListModule,
        MatGridListModule,
        MatButtonModule,
        MatIconModule,
    ],
    providers: [
        QcExperienceService,
        ExperienceResolver
    ],
})
export class QcExperienceModule {
}
