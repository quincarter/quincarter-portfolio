import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SkillsService {

    private _skills: BehaviorSubject<any> = new BehaviorSubject(null);
    public readonly skills$: Observable<any> = this._skills.asObservable();

    constructor(
        private db: AngularFirestore
    ) {
    }

    public getSkills(): void {
        this._skills.next(this.db.collection('/skills').valueChanges());
    }
}
