import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcProfileCardComponent } from './qc-profile-card.component';
import { CommonModule } from '@angular/common';
import { ClipboardModule } from 'ngx-clipboard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const profileMock = {
    profile: {
        contact: {
            email: 'test@test.com',
            github: 'https://github.com/usernameHere',
            gitlab: 'https://gitlab.com/usernameHere',
            linkedin: 'https://linkedin.com/in/usernameHere',
            phone: '(123) 456-7890',
            phone_just_numbers: '1234567890'
        },
        name: 'Some Guy',
        tagline: 'Something inspirational',
        title: 'I am super important',
        title_meta: 'I really am'
    }
};

describe('QcProfileCardComponent', () => {
    let component: QcProfileCardComponent;
    let fixture: ComponentFixture<QcProfileCardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                QcProfileCardComponent
            ],
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                ClipboardModule,
                FlexLayoutModule,
                HttpClientModule,
                MatButtonModule,
                MatIconModule,
                MatListModule,
                MatGridListModule,
                MatCardModule,
                MatRippleModule,
                MatSnackBarModule
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QcProfileCardComponent);
        component = fixture.componentInstance;
        component.profile = profileMock.profile;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
