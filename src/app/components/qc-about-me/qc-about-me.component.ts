import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AboutMeService } from '../../services/about-me/about-me.service';
import { routeAnimations } from '../../core/animations/route.animations';

@Component({
    selector: 'qc-about-me',
    templateUrl: './qc-about-me.component.html',
    styleUrls: ['./qc-about-me.component.scss'],
    animations: [routeAnimations]

})
export class QcAboutMeComponent implements OnInit, OnDestroy {

    constructor(
        private aboutMeService: AboutMeService
    ) {
    }

    private aboutMeSubscription: Subscription;

    public aboutMe$: Observable<any>;

    ngOnInit() {
        this.getAboutMeSubscription();
    }

    ngOnDestroy(): void {
        this.aboutMeSubscription.unsubscribe();
    }

    private getAboutMeSubscription(): void {
        this.aboutMeSubscription = this.aboutMeService.aboutMe$.subscribe(data => {
            this.aboutMe$ = data;
        });
    }
}
