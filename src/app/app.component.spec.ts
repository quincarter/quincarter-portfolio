import { async, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { QcNavComponent } from './components/qc-nav/qc-nav.component';
import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule, Routes } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from './core/core.module';

const routes: Routes = [{
    path: '',
    loadChildren: () => import('./components/qc-components.module').then(m => m.QcComponentsModule),
}];

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                QcNavComponent,
            ],
            imports: [
                BrowserModule,
                CoreModule,
                BrowserAnimationsModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
                FlexLayoutModule,
                LayoutModule,
                MatToolbarModule,
                MatButtonModule,
                MatSidenavModule,
                MatIconModule,
                MatListModule,
                MatGridListModule,
                MatSnackBarModule,
                RouterTestingModule,
                RouterModule.forRoot(routes),
            ],
            providers: [{
                provide: APP_BASE_HREF,
                useValue: '/'
            }],
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'Quin Carter - Senior Software Engineer'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('Quin Carter - Senior Software Engineer');
    });
});
