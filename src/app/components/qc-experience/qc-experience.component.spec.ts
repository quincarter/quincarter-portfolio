import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcExperienceComponent } from './qc-experience.component';
import { CommonModule } from '@angular/common';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ExperienceResolver } from '@app/components/qc-experience/experience.resolver';
import { QcExperienceService } from '@app/services/experience/about-me.service';

describe('QcExperienceComponent', () => {
    let component: QcExperienceComponent;
    let fixture: ComponentFixture<QcExperienceComponent>;

    const routes: Routes = [
        {
            path: 'career',
            component: QcExperienceComponent,
            resolve: {
                resolver: ExperienceResolver
            },
        }
    ];

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                QcExperienceComponent
            ],
            imports: [
                CommonModule,
                AngularFirestoreModule,
                RouterModule.forChild(routes),
                MatCardModule,
                ScrollingModule,
                MatListModule,
                MatGridListModule,
                MatButtonModule,
                MatIconModule,
            ],
            providers: [
                QcExperienceService,
                ExperienceResolver
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QcExperienceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
