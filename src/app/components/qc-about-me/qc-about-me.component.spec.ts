import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcAboutMeComponent } from './qc-about-me.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AboutMeResolver } from './about-me.resolver';
import { AboutMeService } from '../../services/about-me/about-me.service';

describe('QcAboutMeComponent', () => {
    let component: QcAboutMeComponent;
    let fixture: ComponentFixture<QcAboutMeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [QcAboutMeComponent],
            imports: [
                AngularFireModule.initializeApp(environment.firebase),
                AngularFirestoreModule,
                CommonModule,
                FlexLayoutModule,
                HttpClientModule,
                MatButtonModule,
                MatIconModule,
                MatListModule,
                MatGridListModule,
                MatCardModule,
                MatRippleModule,
            ],
            providers: [
                AboutMeResolver,
                AboutMeService
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QcAboutMeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
